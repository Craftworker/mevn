import Vue from 'vue';
import Router from 'vue-router';

import MainPage from '../components/MainPage';
import CreatePostForm from '../components/CreatePostForm';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage,
    },
    {
      path: '/add',
      name: 'CreatePost',
      component: CreatePostForm,
    },
    {
      path: '/edit/:id',
      name: 'EditPost',
      component: CreatePostForm,
    },
  ],
});
